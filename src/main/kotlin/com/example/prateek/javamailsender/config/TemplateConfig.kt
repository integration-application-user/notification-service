package com.example.prateek.javamailsender.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.mail.SimpleMailMessage

@Configuration
class TemplateConfig {

    @Bean
    fun exampleNewsLetter():SimpleMailMessage{
        val template =SimpleMailMessage()

        template.setSubject("NewsLetter")
        template.setText("""
             Hello %s, 
            
            This is an example newsletter message
""".trimIndent())
        return template
    }
}
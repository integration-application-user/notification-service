package com.example.prateek.javamailsender

class EmailRequest(
        val subject: String?,
        val targetEmail : String?,
        val text: String?,
        val name: String?
)
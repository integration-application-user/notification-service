package com.example.prateek.javamailsender.controller

import com.example.prateek.javamailsender.EmailRequest
import com.example.prateek.javamailsender.service.EmailSenderService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class TestController(
        private val emailSenderService: EmailSenderService
) {
    @PostMapping("/api/email")
    fun sendSimpleMail(@RequestBody request: EmailRequest) : ResponseEntity<String>{
        emailSenderService.sendMail(
                subject = request.subject!!,
                text = request.text!!,
                targetEmail = request.targetEmail!!
        )
        return ResponseEntity.noContent().build()
    }

    @PostMapping("/api/email/template")
    fun sendSimpleTemplateEmail(
            @RequestBody request: EmailRequest
    ):ResponseEntity<String>{
        emailSenderService.sendEmailUsingTemplate(
                name = request.name!!,
                targetEmail = request.targetEmail!!
        )
        return ResponseEntity.noContent().build()
    }

    @PostMapping("/api/email/attachment")
    fun sendEmailWithAttachment(
            @RequestBody request: EmailRequest
    ): ResponseEntity<String> {
        emailSenderService.sendEmailWithAttachment(
                targetEmail = request.targetEmail!!
        )
        return ResponseEntity.noContent().build()
    }
}

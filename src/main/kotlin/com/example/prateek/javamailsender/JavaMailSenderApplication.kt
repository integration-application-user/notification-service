package com.example.prateek.javamailsender

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JavaMailSenderApplication

fun main(args: Array<String>) {
	runApplication<JavaMailSenderApplication>(*args)
}
